import dill
import requests
from multiprocessing.dummy import Pool


class Musician:

    def __init__(self, conductor, url):
        self.conductor = conductor
        self.url = url
        self.available = True

    def __enter__(self):
        self.available = False
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.available = True


class Conductor:

    musicians = []

    def add_musician_url(self, url):
        self.musicians.append(Musician(self, url))

    def get_musician(self):
        i = 0
        while True:
            m = self.musicians[i % len(self.musicians)]
            if m.available:
                m.available = False
                return m
            i += 1

    def perform(self, func, args):
        with self.get_musician() as musician:
            return dill.loads(requests.post(
                url=musician.url + '/perform',
                stream=True,
                headers={'Content-Type': 'application/octet-stream'},
                data=dill.dumps({'function': func, 'arguments': args})
            ).content)

    def perform_tuple(self, payload):
        return self.perform(payload[0], payload[1])

    def conduct(self, tuples):
        with Pool(len(self.musicians) + 1) as pool:
            return pool.map(self.perform_tuple, tuples)



