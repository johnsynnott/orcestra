import dill
import flask
import sys

app = flask.Flask(__name__)


@app.route('/perform', methods=['POST'])
def perform():
    payload = dill.loads(flask.request.get_data())
    return dill.dumps(payload['function'](*payload['arguments']) if payload['arguments'] is not None else payload['function']())


app.run(host='0.0.0.0', port=sys.argv[1])
