# This file is designed to be run from an iPython shell, or imported into a regular python shell.

from conductor import Conductor

c = Conductor()

[c.add_musician_url(f"http://127.0.0.1:{port}") for port in range(5000, 1 + 5005)]


def wait(t):
    from time import sleep
    sleep(t)
    return t


def pid():
    import os
    return os.getpid()
