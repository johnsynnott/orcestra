import dill
import itertools
from multiprocessing.dummy import Pool
import socket
import zlib


CHUNK_SIZE = 4096


class Worker:
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.available = True
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((self.host, self.port))

    def __enter__(self):
        self.available = False
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.available = True

    def close(self):
        self.sock.close()

    @staticmethod
    def recv_all(sock):
        chunks = b''
        chunk = list(range(CHUNK_SIZE))
        while len(chunk) == CHUNK_SIZE:
            chunk = sock.recv(CHUNK_SIZE)
            chunks = chunks + chunk
        return chunks

    # Expects a dict of the form {'function': func, 'arguments': (1,2,3)}
    def run(self, payload):
        self.sock.sendall(zlib.compress(dill.dumps(payload), 9))
        return dill.loads(zlib.decompress(Worker.recv_all(self.sock), 15 + 32))


class NetworkPool:
    workers = []

    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_val, exc_tb):
        for worker in self.workers:
            worker.close()

    def add_worker(self, host, port):
        self.workers.append(Worker(host, port))

    def get_worker(self):
        for i in itertools.count():
            worker = self.workers[i % len(self.workers)]
            if worker.available:
                worker.available = False
                return worker

    # Expects a dict of the form {'function': func, 'arguments': (1,2,3)}
    def run_payload(self, payload):
        with self.get_worker() as worker:
            return worker.run(payload)

    def map(self, func, arg_tuples):
        with Pool(len(self.workers) + 1) as pool:
            return pool.map(self.run_payload, ({'function': func, 'arguments': args} for args in arg_tuples))




