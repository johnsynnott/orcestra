import network_pool
from pprint import pprint as pp
from time import sleep

np = network_pool.NetworkPool()

for i in range(5000, 1 + 5008):
    np.add_worker('127.0.0.1', i)


def wait(t):
    from time import sleep
    sleep(t)
    return t


def wget(num):
    from requests import get
    with open(f"downloads/{num}.txt", 'w') as f:
        f.write(get(f"http://127.0.0.1:8080/num/{num}").text)
    return True


pp(np.map(wget, [(x,) for x in range(1000)]))
