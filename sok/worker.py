import dill
import datetime
import socketserver
import zlib


def timestamp():
    return str(datetime.datetime.now())


class WorkerHandler(socketserver.BaseRequestHandler):

    chunk_size = 4096

    def recv_all(self):
        chunks = b''
        chunk = list(range(self.chunk_size))
        while len(chunk) >= self.chunk_size:
            chunk = self.request.recv(self.chunk_size)
            chunks = chunks + chunk
        return chunks

    def handle(self):
        pool_addr = ':'.join(str(x) for x in self.client_address)
        print(f"[{timestamp()}] - connection from {pool_addr}")
        while True:
            payload = dill.loads(zlib.decompress(self.recv_all(), 15 + 32))
            print(f"[{timestamp()}] - payload from {pool_addr}")
            self.request.sendall(
                zlib.compress(
                    dill.dumps(
                        payload['function'](*payload['arguments'])
                        if payload['arguments'] is not None else
                        payload['function']()
                    )
                    , 9
                )
            )


if __name__ == "__main__":
    port = 5000
    die = False
    while not die:
        try:
            with socketserver.TCPServer(('0.0.0.0', port), WorkerHandler) as server:
                print(f"Started on {port}")
                die = True
                server.serve_forever()
        except OSError:
            port += 1
